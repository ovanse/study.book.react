import React from 'react';

const tahoe_peaks = [
  {
    name: 'Freel Peak',
    elevation: 10891,
  },
  {
    name: 'Monument Peak',
    elevation: 10067,
  },
  {
    name: 'Pyramid Peak',
    elevation: 9983,
  },
  {
    name: 'Mt. Tallac',
    elevation: 9735,
  },
];

function List({ data = [], renderEmpty, renderItem }) {
  if (!data.length) return renderEmpty;
  return (
    <>
      <p>{data.length} items</p>
      <ul className='list-group'>
        {data.map((item, i) => (
          <li key={i} className='list-group-item'>
            {renderItem(item)}
          </li>
        ))}
      </ul>
    </>
  );
}

function App() {
  return (
    <div className='container'>
      <List
        data={tahoe_peaks}
        renderEmpty={<p>This list is empty</p>}
        renderItem={(item) => (
          <>
            {item.name} - {item.elevation.toLocaleString()}ft
          </>
        )}
      />
    </div>
  );
}

export default App;
