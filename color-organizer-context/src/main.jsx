import React, { createContext } from 'react';
import ReactDOM from 'react-dom/client';
import App from './App.jsx';
import { ColorProvider } from './contexts/color-hooks.jsx';

export const ColorContext = createContext();

ReactDOM.createRoot(document.getElementById('root')).render(
  <ColorProvider>
    <App />
  </ColorProvider>
);
