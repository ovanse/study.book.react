import { FaStar } from 'react-icons/fa';

export default function Star({ selected = false, onSelect = (f) => f }) {
  return (
    <FaStar
      style={{ cursor: 'pointer' }}
      color={selected ? 'gold' : 'grey'}
      onClick={onSelect}
    />
  );
}
