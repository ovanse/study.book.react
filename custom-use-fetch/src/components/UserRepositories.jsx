import React from 'react';
import RepoMenu from './RepoMenu';
import Fetch from './Fetch';

const UserRepositories = ({ login, selectedRepo, onSelect = (f) => f }) => {
  return (
    <Fetch
      uri={`https://api.github.com/users/${login}/repos`}
      renderSuccess={({ data }) => (
        <RepoMenu
          repositories={data}
          selectedRepo={selectedRepo}
          onSelect={onSelect}
          login={login}
        />
      )}
    />
  );
};

export default UserRepositories;
