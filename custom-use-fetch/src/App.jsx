import React from 'react';
import GitHubUser from './components/GitHubUser';

function App() {
  const [login] = React.useState('ovanse');

  return (
    <>
      <GitHubUser login={login} />
    </>
  );
}

export default App;
