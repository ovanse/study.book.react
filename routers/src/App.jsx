import { Route, Routes, Redirect } from 'react-router-dom';
import {
  About,
  Contact,
  Events,
  History,
  Home,
  Location,
  Products,
  Services,
  Whoops404,
} from './pages';

const App = () => {
  return (
    <div>
      <Routes>
        <Route path='/' element={<Home />} />
        <Redirect from='services' to='about/services' />
        <Route path='about' element={<About />}>
          <Route path='services' element={<Services />} />
          <Route path='history' element={<History />} />
          <Route path='location' element={<Location />} />
        </Route>
        <Route path='events' element={<Events />} />
        <Route path='products' element={<Products />} />
        <Route path='contact' element={<Contact />} />
        <Route path='*' element={<Whoops404 />} />
      </Routes>
    </div>
  );
};

export default App;
