import { createRoot } from "react-dom/client";
import PropTypes from "prop-types";

function App({ status }) {
  return (
    <div>
      <h1>We're {status === "Open" ? "Open!" : "Closed!"}</h1>
    </div>
  );
}

App.propTypes = {
  status: PropTypes.oneOf(["Open", "Closed"]).isRequired
};

createRoot(document.getElementById("root")).render(<App status="Open" />);
